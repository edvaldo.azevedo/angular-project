import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms'
import { Router } from '@angular/router';

@Component({
  selector: 'app-cadastro-clientes',
  templateUrl: './cadastro-clientes.component.html',
  styleUrls: ['./cadastro-clientes.component.css']
})
export class CadastroClientesComponent implements OnInit {

  formCadastro:FormGroup;
  conversao:any;

  constructor(private fb: FormBuilder, private router:Router) { }

  ngOnInit(): void {

    this.formCadastro = this.fb.group({
      nome: [''],
      cpf: [],
      email: [],
      telefone: [],
      endereco: []
    });
  }

  cadastro(){
    console.log("Chamou o cadastro" + this.formCadastro.value);

    this.conversao = JSON.stringify(this.formCadastro.value);
    localStorage.setItem('cadastro', this.conversao);
    this.router.navigate(['/cadastro-concluido']);
  }

}
